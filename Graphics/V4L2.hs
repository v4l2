{- |
Module      : Graphics.V4L2
Maintainer  : claude@mathr.co.uk
Stability   : no
Portability : no
-}
module Graphics.V4L2 (
    module Graphics.V4L2.Device
  , module Graphics.V4L2.Capability
  , module Graphics.V4L2.Priority
  , module Graphics.V4L2.Control
  , module Graphics.V4L2.Field
  , module Graphics.V4L2.PixelFormat
  , module Graphics.V4L2.ColorSpace
  , module Graphics.V4L2.Format
  , module Graphics.V4L2.VideoInput
  , module Graphics.V4L2.VideoStandard
  , module Graphics.V4L2.VideoCapture
  ) where

import Graphics.V4L2.Device
import Graphics.V4L2.Capability
import Graphics.V4L2.Priority
import Graphics.V4L2.Control
import Graphics.V4L2.Field
import Graphics.V4L2.PixelFormat
import Graphics.V4L2.ColorSpace
import Graphics.V4L2.Format
import Graphics.V4L2.VideoInput
import Graphics.V4L2.VideoStandard
import Graphics.V4L2.VideoCapture

{-
  -- * Buffers and formats.
  , BufferType(..)
  -- ** Image formats.
  -- * Audio inputs.
  , AudioIndex
  -- * Tuners.
  , TunerIndex
  -- * Exceptions
  , V4L2Bug(..)
  ) where
import Prelude hiding (null, catch)
import Foreign
import Foreign.C
import Foreign.Helper
import Control.Monad (when)
import Control.Exception
import GHC.IO.Exception (IOErrorType(InvalidArgument, ResourceBusy), ioe_type)
import System.Posix.Types (Fd(Fd), CMode)
import System.Posix.IOCtl (IOControl(ioctlReq))
import Data.Set (Set, empty, fromList, singleton, union, insert, null)
import Data.Word (Word8, Word32, Word64)
import Data.Bits (shiftR, (.&.), testBit)
import Data.Maybe (fromMaybe)
import Bindings.Linux.VideoDev2
import Bindings.LibV4L2

{- | Buffer type identifiers. -}
enum "BufferType" "C'v4l2_buf_type" "c'V4L2_BUF_TYPE_" "B" $ words "VIDEO_CAPTURE VIDEO_CAPTURE_MPLANE VIDEO_OUTPUT VIDEO_OUTPUT_MPLANE VIDEO_OVERLAY VBI_CAPTURE VBI_OUTPUT SLICED_VBI_CAPTURE SLICED_VBI_OUTPUT VIDEO_OUTPUT_OVERLAY"

{- | Audio input index. -}
newtype AudioIndex = AudioIndex Int
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Data, Typeable, Num, Integral, Real)

{- | Tuner index. -}
newtype TunerIndex = TunerIndex Int
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Data, Typeable, Num, Integral, Real)


zero :: Storable a => IO a
zero = alloca $ \p -> c'memset p 0 (fromIntegral $ sizeOf (undefined `asTypeOf` unsafePerformIO (peek p))) >> peek p

{- | Internal errors.  Please report these as bugs. -}
data V4L2Bug
  = V4L2BugUnparseableInput C'v4l2_input
  | V4L2BugUnparseableFormat C'v4l2_format
  | V4L2BugUnparseableFormatDescription C'v4l2_fmtdesc
  | V4L2BugUnexpectedFrameSizeType C'v4l2_frmsizeenum
  | V4L2BugUnexpectedFrameIntervalType C'v4l2_frmivalenum
  deriving (Show, Typeable)
instance Exception V4L2Bug
-}
