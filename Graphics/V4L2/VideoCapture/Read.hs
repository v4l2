{- |
Module      : Graphics.V4L2.VideoCapture.Read
Maintainer  : claude@mathr.co.uk
Stability   : no
Portability : no

Video capture using Read I/O.
-}
module Graphics.V4L2.VideoCapture.Read
  ( withFrame
  ) where

import Foreign (Ptr, allocaBytes)
import Foreign.C (throwErrnoIfMinus1)
import Bindings.LibV4L2 (c'v4l2_read)

import Graphics.V4L2.Device (Device)
import Graphics.V4L2.Format (ImageFormat, imageSize)

{- |  Capture a video frame. -}
withFrame :: Device -> ImageFormat -> (Ptr a -> Int -> IO b) -> IO b
withFrame d f a = do
  allocaBytes (imageSize f) $ \p -> do
    n <- throwErrnoIfMinus1 "Graphics.V4L2.VideoCapture.Read.withFrame" $
            c'v4l2_read (fromIntegral d) p (fromIntegral $ imageSize f)
    a p (fromIntegral n)
