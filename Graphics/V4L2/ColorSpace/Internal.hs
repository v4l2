{-# LANGUAGE DeriveDataTypeable #-}
{- |
Module      : Graphics.V4L2.ColorSpace.Internal
Maintainer  : claude@mathr.co.uk
Stability   : no
Portability : no
-}
module Graphics.V4L2.ColorSpace.Internal
  ( ColorSpace(..)
  , fromColorSpace
  , toColorSpace
  ) where

import Data.Data (Data)
import Data.Typeable (Typeable)
import Data.Word (Word32)

import Bindings.Linux.VideoDev2

import Foreign.Extra.CEnum (toCEnum, fromCEnum)

{- |  Color spaces. -}
data ColorSpace
  = ColorSMPTE170M
	| ColorSMPTE240M
	| ColorREC709
	| ColorBT878
	| Color470SystemM
	| Color470SystemBG
	| ColorJPEG
	| ColorSRGB
  | ColorUnknown Word32
  deriving (Eq, Ord, Read, Show, Data, Typeable)

{- |  internal -}
fromColorSpace :: C'v4l2_colorspace -> ColorSpace
{- |  internal -}
toColorSpace :: ColorSpace -> C'v4l2_colorspace

(fromColorSpace, toColorSpace) = (fromCEnum spec (ColorUnknown . fromIntegral), toCEnum spec isUnknown unUnknown) where
  spec =
    [ ( ColorSMPTE170M   , c'V4L2_COLORSPACE_SMPTE170M     )
    , ( ColorSMPTE240M   , c'V4L2_COLORSPACE_SMPTE240M     )
    , ( ColorREC709      , c'V4L2_COLORSPACE_REC709        )
    , ( ColorBT878       , c'V4L2_COLORSPACE_BT878         )
    , ( Color470SystemM  , c'V4L2_COLORSPACE_470_SYSTEM_M  )
    , ( Color470SystemBG , c'V4L2_COLORSPACE_470_SYSTEM_BG )
    , ( ColorJPEG        , c'V4L2_COLORSPACE_JPEG          )
    , ( ColorSRGB        , c'V4L2_COLORSPACE_SRGB          )
    ]
  isUnknown (ColorUnknown _) = True
  isUnknown _ = False
  unUnknown (ColorUnknown f) = fromIntegral f
  unUnknown _ = error "Graphice.V4L2.ColorSpace.Internal.toColorSpace"
