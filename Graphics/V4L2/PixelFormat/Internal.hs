{-# LANGUAGE DeriveDataTypeable #-}
{- |
Module      : Graphics.V4L2.PixelFormat.Internal
Maintainer  : claude@mathr.co.uk
Stability   : no
Portability : no
-}
module Graphics.V4L2.PixelFormat.Internal
  ( PixelFormat(..)
  , fromPixelFormat
  , toPixelFormat
  ) where

import Data.Data (Data)
import Data.Typeable (Typeable)
import Data.Word (Word32)

import Bindings.Linux.VideoDev2

import Foreign.Extra.CEnum (toCEnum, fromCEnum)

{- |  Pixel formats. -}
data PixelFormat
  -- grep PIX /usr/include/linux/videodev2.h | sed "s|^#define \(V4L2_PIX_FMT_\([^ \t]*\)\)[ \t].*$|  \| Pixel\2|"
  = PixelRGB332
  | PixelRGB444
  | PixelRGB555
  | PixelRGB565
  | PixelRGB555X
  | PixelRGB565X
  | PixelBGR24
  | PixelRGB24
  | PixelBGR32
  | PixelRGB32
  | PixelGREY
  | PixelY16
  | PixelPAL8
  | PixelYVU410
  | PixelYVU420
  | PixelYUYV
  | PixelYYUV
  | PixelYVYU
  | PixelUYVY
  | PixelVYUY
  | PixelYUV422P
  | PixelYUV411P
  | PixelY41P
  | PixelYUV444
  | PixelYUV555
  | PixelYUV565
  | PixelYUV32
  | PixelYUV410
  | PixelYUV420
  | PixelHI240
  | PixelHM12
  | PixelNV12
  | PixelNV21
  | PixelNV16
  | PixelNV61
  | PixelSBGGR8
  | PixelSGBRG8
  | PixelSGRBG8
  | PixelSGRBG10
  | PixelSGRBG10DPCM8
  | PixelSBGGR16
  | PixelMJPEG
  | PixelJPEG
  | PixelDV
  | PixelMPEG
  | PixelWNVA
  | PixelSN9C10X
  | PixelSN9C20X_I420
  | PixelPWC1
  | PixelPWC2
  | PixelET61X251
  | PixelSPCA501
  | PixelSPCA505
  | PixelSPCA508
  | PixelSPCA561
  | PixelPAC207
  | PixelMR97310A
  | PixelSQ905C
  | PixelPJPG
  | PixelOV511
  | PixelOV518
  | PixelUnknown Word32
  deriving (Eq, Ord, Read, Show, Data, Typeable)

{- |  internal -}
fromPixelFormat :: Word32 -> PixelFormat
{- |  internal -}
toPixelFormat :: PixelFormat -> Word32

(fromPixelFormat, toPixelFormat) = (fromCEnum spec PixelUnknown, toCEnum spec isUnknown unUnknown) where
  -- grep PIX /usr/include/linux/videodev2.h | sed "s|^#define \(V4L2_PIX_FMT_\([^ \t]*\)\)[ \t].*$| , ( Pixel\2 , c'\1 )|"
  spec =
    [ ( PixelRGB332 , c'V4L2_PIX_FMT_RGB332 )
    , ( PixelRGB444 , c'V4L2_PIX_FMT_RGB444 )
    , ( PixelRGB555 , c'V4L2_PIX_FMT_RGB555 )
    , ( PixelRGB565 , c'V4L2_PIX_FMT_RGB565 )
    , ( PixelRGB555X , c'V4L2_PIX_FMT_RGB555X )
    , ( PixelRGB565X , c'V4L2_PIX_FMT_RGB565X )
    , ( PixelBGR24 , c'V4L2_PIX_FMT_BGR24 )
    , ( PixelRGB24 , c'V4L2_PIX_FMT_RGB24 )
    , ( PixelBGR32 , c'V4L2_PIX_FMT_BGR32 )
    , ( PixelRGB32 , c'V4L2_PIX_FMT_RGB32 )
    , ( PixelGREY , c'V4L2_PIX_FMT_GREY )
    , ( PixelY16 , c'V4L2_PIX_FMT_Y16 )
    , ( PixelPAL8 , c'V4L2_PIX_FMT_PAL8 )
    , ( PixelYVU410 , c'V4L2_PIX_FMT_YVU410 )
    , ( PixelYVU420 , c'V4L2_PIX_FMT_YVU420 )
    , ( PixelYUYV , c'V4L2_PIX_FMT_YUYV )
    , ( PixelYYUV , c'V4L2_PIX_FMT_YYUV )
    , ( PixelYVYU , c'V4L2_PIX_FMT_YVYU )
    , ( PixelUYVY , c'V4L2_PIX_FMT_UYVY )
    , ( PixelVYUY , c'V4L2_PIX_FMT_VYUY )
    , ( PixelYUV422P , c'V4L2_PIX_FMT_YUV422P )
    , ( PixelYUV411P , c'V4L2_PIX_FMT_YUV411P )
    , ( PixelY41P , c'V4L2_PIX_FMT_Y41P )
    , ( PixelYUV444 , c'V4L2_PIX_FMT_YUV444 )
    , ( PixelYUV555 , c'V4L2_PIX_FMT_YUV555 )
    , ( PixelYUV565 , c'V4L2_PIX_FMT_YUV565 )
    , ( PixelYUV32 , c'V4L2_PIX_FMT_YUV32 )
    , ( PixelYUV410 , c'V4L2_PIX_FMT_YUV410 )
    , ( PixelYUV420 , c'V4L2_PIX_FMT_YUV420 )
    , ( PixelHI240 , c'V4L2_PIX_FMT_HI240 )
    , ( PixelHM12 , c'V4L2_PIX_FMT_HM12 )
    , ( PixelNV12 , c'V4L2_PIX_FMT_NV12 )
    , ( PixelNV21 , c'V4L2_PIX_FMT_NV21 )
    , ( PixelNV16 , c'V4L2_PIX_FMT_NV16 )
    , ( PixelNV61 , c'V4L2_PIX_FMT_NV61 )
    , ( PixelSBGGR8 , c'V4L2_PIX_FMT_SBGGR8 )
    , ( PixelSGBRG8 , c'V4L2_PIX_FMT_SGBRG8 )
    , ( PixelSGRBG8 , c'V4L2_PIX_FMT_SGRBG8 )
    , ( PixelSGRBG10 , c'V4L2_PIX_FMT_SGRBG10 )
    , ( PixelSGRBG10DPCM8 , c'V4L2_PIX_FMT_SGRBG10DPCM8 )
    , ( PixelSBGGR16 , c'V4L2_PIX_FMT_SBGGR16 )
    , ( PixelMJPEG , c'V4L2_PIX_FMT_MJPEG )
    , ( PixelJPEG , c'V4L2_PIX_FMT_JPEG )
    , ( PixelDV , c'V4L2_PIX_FMT_DV )
    , ( PixelMPEG , c'V4L2_PIX_FMT_MPEG )
    , ( PixelWNVA , c'V4L2_PIX_FMT_WNVA )
    , ( PixelSN9C10X , c'V4L2_PIX_FMT_SN9C10X )
    , ( PixelSN9C20X_I420 , c'V4L2_PIX_FMT_SN9C20X_I420 )
    , ( PixelPWC1 , c'V4L2_PIX_FMT_PWC1 )
    , ( PixelPWC2 , c'V4L2_PIX_FMT_PWC2 )
    , ( PixelET61X251 , c'V4L2_PIX_FMT_ET61X251 )
    , ( PixelSPCA501 , c'V4L2_PIX_FMT_SPCA501 )
    , ( PixelSPCA505 , c'V4L2_PIX_FMT_SPCA505 )
    , ( PixelSPCA508 , c'V4L2_PIX_FMT_SPCA508 )
    , ( PixelSPCA561 , c'V4L2_PIX_FMT_SPCA561 )
    , ( PixelPAC207 , c'V4L2_PIX_FMT_PAC207 )
    , ( PixelMR97310A , c'V4L2_PIX_FMT_MR97310A )
    , ( PixelSQ905C , c'V4L2_PIX_FMT_SQ905C )
    , ( PixelPJPG , c'V4L2_PIX_FMT_PJPG )
    , ( PixelOV511 , c'V4L2_PIX_FMT_OV511 )
    , ( PixelOV518 , c'V4L2_PIX_FMT_OV518 )
    ]
  isUnknown (PixelUnknown _) = True
  isUnknown _ = False
  unUnknown (PixelUnknown f) = f
  unUnknown _ = error "Graphice.V4L2.PixelFormat.Internal.toPixelFormat"
