{-# LANGUAGE DeriveDataTypeable #-}
{- |
Module      : Graphics.V4L2.Priority
Maintainer  : claude@mathr.co.uk
Stability   : no
Portability : no
-}
module Graphics.V4L2.Priority
  ( Priority(..)
  , getPriority
  , setPriority
  ) where

import Data.Data (Data)
import Data.Typeable (Typeable)
import Data.Word (Word32)

import Bindings.Linux.VideoDev2

import Foreign.Extra.CEnum (fromCEnum, toCEnum)
import Graphics.V4L2.Device (Device)
import Graphics.V4L2.IOCtl (ioctl', ioctl_)

{- |  Priorities. -}
data Priority
  = PriorityUnset
  | PriorityBackground
  | PriorityInteractive
  | PriorityDefault
  | PriorityRecord
  | PriorityUnknown Word32
  deriving (Eq, Ord, Read, Show, Data, Typeable)

fromPriority :: C'v4l2_priority -> Priority
toPriority :: Priority -> C'v4l2_priority
(fromPriority, toPriority) = (fromCEnum spec (PriorityUnknown . fromIntegral), toCEnum spec isUnknown unUnknown) where
  spec =
    [ ( PriorityUnset       , c'V4L2_PRIORITY_UNSET       )
    , ( PriorityBackground  , c'V4L2_PRIORITY_BACKGROUND  )
    , ( PriorityInteractive , c'V4L2_PRIORITY_INTERACTIVE )
    , ( PriorityDefault     , c'V4L2_PRIORITY_DEFAULT     )
    , ( PriorityRecord      , c'V4L2_PRIORITY_RECORD      )
    ]
  isUnknown (PriorityUnknown _) = True
  isUnknown _ = False
  unUnknown (PriorityUnknown x) = fromIntegral x
  unUnknown _ = error "Graphics.V4L2.Priority.toPriority.unUnknown"

{- |  Get priority. -}
getPriority :: Device -> IO Priority
getPriority d = fromPriority `fmap` ioctl' d C'VIDIOC_G_PRIORITY

{- |  Set priority. -}
setPriority :: Device -> Priority -> IO ()
setPriority d p = ioctl_ d C'VIDIOC_S_PRIORITY (toPriority p)
