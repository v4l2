{- |
Module      : Graphics.V4L2.VideoCapture
Maintainer  : claude@mathr.co.uk
Stability   : no
Portability : no
-}
module Graphics.V4L2.VideoCapture
  ( module Graphics.V4L2.VideoCapture.Read
  ) where

import Graphics.V4L2.VideoCapture.Read
