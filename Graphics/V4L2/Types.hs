{- |
Module      : Graphics.V4L2.Types
Maintainer  : claude@mathr.co.uk
Stability   : no
Portability : no
-}
module Graphics.V4L2.Types
  ( module Graphics.V4L2.Types.Internal
  ) where

import Graphics.V4L2.Types.Internal
  ( Fraction(..)
  )
