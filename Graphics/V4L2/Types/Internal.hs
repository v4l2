{-# LANGUAGE DeriveDataTypeable #-}
{- |
Module      : Graphics.V4L2.Types.Internal
Maintainer  : claude@mathr.co.uk
Stability   : no
Portability : no
-}
module Graphics.V4L2.Types.Internal
  ( Fraction(..)
  , fromFraction
  ) where

import Data.Data (Data)
import Data.Word (Word32)
import Data.Typeable (Typeable)

import Bindings.Linux.VideoDev2

{- |  Fraction type. -}
data Fraction = Fraction{ fractionNumerator, fractionDenominator:: Word32 }
  deriving (Eq, Ord, Read, Show, Data, Typeable)

{- |  Unmarshal fraction. -}
fromFraction :: C'v4l2_fract -> Fraction
fromFraction f = Fraction
  { fractionNumerator = c'v4l2_fract'numerator f
  , fractionDenominator = c'v4l2_fract'denominator f
  }
