module Foreign.Extra.String
  ( fromString
  ) where

import Data.Word (Word8)

fromString :: [Word8] -> String
fromString = map (toEnum . fromEnum) . takeWhile (/= 0)
