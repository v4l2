module Foreign.Extra.CEnum
  ( fromCEnum
  , toCEnum
  ) where

fromCEnum :: (Ord h, Eq c) => [(h, c)] -> (c -> h) -> c -> h
fromCEnum spec unknown enum = case enum `lookup` map swap spec of
    Nothing -> unknown enum
    Just h -> h
  where
    swap (a, b) = (b, a)

toCEnum :: Eq h => [(h, c)] -> (h -> Bool) -> (h -> c) -> h -> c
toCEnum spec isUnknown unUnknown h
  | isUnknown h = unUnknown h
  | otherwise = case h `lookup` spec of
      Nothing -> error "toCEnum"
      Just c -> c
